--[[ route 
Note: to use routing do the following in the main application module:

-- attach route module:
require 'route'

-- ...

-- set up routes:
route('/', {'post', 'get'}, handler1)
route('/a/b/c/d', {'get'}, handler2)
-- ...
-- or:
route.assign('/', {'post', 'get'}, handler1)
route.assign('/a/b/c/d', {'get'}, handler2)

-- remenber that 'route' redefines global "application" variable to itself to intercept subsequent "application.execute()" call(s).
]]

route = {}

route.tree={}

--[[list has hierarchical structure like this:
    {handlers={}, subpath={path1={ handlers..., subpath... }, path2={...}}}
    
    *------[handlers]--+---[get]
        |                        |
        |                       +---[put]
        ...                       ...
        |
       +---[subpath]---+---[<path 1>]---+---[handlers]---+---[get]....
                                 |                          |                         
                                 |                         +---[subpath]----+---[<path 1 1>]...
                                 |                                                    |
                                 |                                                   +---...
                                 +--[<path 2>]----....
    { 
        handlers:{
            put: <function>,
            post: <function>,
            get: <function>,
            any: <function>,
            head: <function>,
            delete: <function>
        },
        subpath: {
            arg1_1: {
                    handlers:{
                        put: <function>,
                        post: <function>,
                        get: <function>,
                        any: <function>,
                        head: <function>,
                        delete: <function>
                    },
                subpath: {
                    put
                }
            },
            arg1_2:{...}
            
        }
    }
    
    if path exactly "arg1/arg2", then child handler must be called, otherwise - go to the nested tables  

]] 

require 'utils'

local function addnode( htable, urntags )
    --[[
    Create, if necessary, correspondend node denoted by path (ars[1]/urntags[2].../urntags[n])
    args: 
    htable: hierarchical table of routes
    urntags: indexed table of path portions
    @ returns: reference to specified table node (newly created node is empty table, without "handlers" and "subpath"!)
    ]]
    local node = htable
    for _, tag in ipairs(urntags) do
--~         node.subpath = node.subpath or {}
--~         node.subpath[ tag ] = node.subpath[ tag ] or {} 
        if not node.subpath then node.subpath = {} end
        if not node.subpath[tag] then node.subpath[tag]={} end
        node=node.subpath[ tag ]
    end
    return node
end

local function findnode( htable, urntags )
    --[[
    Search correspondend node denoted by path (ars[1]/urntags[2].../urntags[n])
    args: 
    htable: hierarchical table of routes
    urntags: indexed table of path portions
    @ returns: reference to table node of pair founded or nil if node for path doesn't  exist....
    ]]
    local node = htable
    for _, tag in ipairs(urntags) do
--~         node = node.subpath and node.subpath[tag]
        node = node and node.subpath[tag]
        if not node then return nil end -- break on non-existed node
    end
    return node
end

function route.assign( records )  -- each item of "records" must be array: [1:urn, 2: httpmethods, 3:handler]
    --[[
    Assign handler to one or more http methods for specified urn
    ]]
    for _, item in ipairs( records ) do
        -- expand args:
        local urn, httpmethods, handler  = unpack( item )
        httpmethods = httpmethods or {'get'}    
        
    --~     print('urn=\"'..(urn or 'None')..'\"; httpmethods: '..tostring(httpmethods)..'; handler: '..tostring(handler))    

        assert( type(httpmethods) == 'table', 'Route methods must be a table, but type is: '.. type(httpmethods)..'\", urn: \"'.. urn ..'\"' )
        assert( type(urn) == 'string', 'URN must be a string like "/path1/path2/.../pathN"} or empty table {} for default root handler, but type is: '..type(urn))
        assert( type(handler) == 'function', 'Handler must be a \"function\", but type is: \"'.. type(handler)..'\", urn: \"'.. urn ..'\"' )
        
        local trimslash, split = utils.uri.trimslash, utils.uri.split
        
        local urntags = split( trimslash( urn ) )
        
        local node = addnode(route.tree, urntags)
    --~     local isnew = (node.handlers == nil)
    --~     if isnew then node.handlers={} end
        
        if not node.handlers then node.handlers = {} end
         
        for i=1,#httpmethods do
            local methodname = httpmethods[i]:lower()
            assert( not node.handlers[methodname], 'Handler for \"/'..table.concat(urntags, '/')..'\" and method \"'.. methodname ..'\" already defined!' )
            node.handlers[ methodname ] = handler
        end    
    end
end

function route.execute( request, response )
    local node = findnode( route.tree, request.args ) 
    if node then 
        local handler = node.handlers and node.handlers[ request.method ]
        if handler then
            return handler(request, response)
        else
            response.notallowed("HTTP method \""..request.method.."\" not allowed for this resource")
        end
    else --node not found
        response.notfound('Page for \"'.. table.concat(request.args,'/')..'\" was not found')
    end
end

function route.dump(delimiter, node, textshift, level)
    delimiter = delimiter or '\n'
    node = node or route.tree
    textshift = textshift and '....' .. textshift or  ''    
    local outstr = ''
    level = level or 1
    
    for key, value in pairs( node ) do
        local line=''
        if type(key) == 'string' then key = '\"'..key..'\"' end
--~         if type(key) == 'string' then key = '\"'..key..'['..level..']'..'\"' end
        if type(value) == 'string' then 
            line = key..':\"'..value..'\"'
        elseif type(value) == 'table' then
            line = key..':'..route.dump(delimiter, value, textshift, level+1 )..'-- endkey: '..key
        elseif type(value) == 'function' then
            line = key..':'..( value and 'handler'or 'None' )
        else
--~             line = key..':' .. 'type:'..type(value)
        end
        outstr = outstr .. delimiter .. textshift .. line
    end
    return '{'..outstr..delimiter..textshift..'}'
end

-- set metamethod for callable protocol
local mt = {}
setmetatable( route,  mt)
mt.__call = function( self, request, response )  self.execute( request, response ) end

--[[
    assign global application variable to route. So application.execute( request, response ) wil actually call route.execute( request, response ) 
    with subsequent calling of handler, registered for urn and http method
]]

return route