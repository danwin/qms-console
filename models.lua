--[[model.lua]]
-- this is application specific file, not from mcgi core!

require'svcwrapper'

cfg=cfg or {}
cfg.aliases=cfg.aliases or {}
assert( pcall(dofile, 'aliases.lua') )
function aliases(s) 	return assert(cfg.aliases[s], ('Alias for \"%s\" was not registered'):format(s) ) end

-- Protocol~s ===================================================================

	Request{ "TEST_SERVICE",
	
		[["This is a
		test service"
		]],
		
		message='BEGIN\r\nThis is test file: [%(arg1)]',
		
		to='c:\\temp\\%(testfile)',
		from='c:\\temp\\%(testfile)',
	}
	
	-- user logon process and server negotiation ---
	
	
	Request{ "CHECK_SERVER_REQUEST",

		from=aliases'X:\\���������\\������.id',
		
		decoder	=function( args, rsobj )
			return ( rsobj ~= nil ) or { lasterror = 1, warning = 'Cannot connect to LCP node' }	
		end
	}--check_server (Start.pas - line 92)
	
	
	Request{ "ENUM_USERS_REQUEST",

		to 			= aliases'X:\\������ ����������\\%(console_name).req',	-- request address
		message 	= 'GETUSERS\r\n%(console_name)\r\nEND',			-- request message template
		from 		= aliases'X:\\������ ����������\\%(console_name).usr',	-- response address
		timeout 	= 2400,
		
		decoder	=function( args, rsobj )
			if rsobj ~= nil then
				return rsobj -- convert file-like object to table of strings
			else
				return { lasterror = 1, warning = 'cannot enumerate users' }
			end
		end
	}--enum_users (Start.pas - line 100)
	
	
	Request{ "GET_CONSOLE_PRIVELEGES_REQUEST",
	
		from=aliases'X:\\������ ����������\\%(console_name).pop',
		
		decoder	=function( args, rsobj )
			if not rsobj then return { lasterror=1, warning='cannot open cosole privileges file' } end
			local lines = rsobj
			local val = lines[#lines] and tonumber( lines[#lines] ) or nil
			-- validate:
			if 21==#lines and val then
				return { allowcallbyid = val == 0 }
			else
				return { lasterror=2, warning='invalid or corrupted console privileges file' }
			end
		end	
	}-- get_console_call_by_id_rights (Start.pas - line 146 - by DVA)
	
	-- login_user (Start.pas - line 208 Button1Click) -- divided into 2 stages - login user (check no .err file), then read user privileges (read .ok file)
	Request{ "USER_LOGIN_REQUEST",
		
		[[
			@return: boolean;
			Note. 
		]], -- description 
		
		to = aliases'X:\\������ ����������\\%(console_name).req', 	-- request address
		message = 'CHECKPASSWORD\r\n%(console_name)\r\n%(user_name)\r\n%(password)\r\nEND',			-- request message template
		from = aliases'X:\\������ ����������\\%(console_name).err',	-- response address
		timeout = 1000,
		
		decoder=function( args, rsobj )
			if rsobj then -- .err file exists so login prohibited !
				return {
					lasterror = 1, -- no such user or incorrect password
					warning = 'no such user or incorrect password'
				}
			else
				return true
			end
		end
	}

	Request{ "USER_PRIVILEGES_REQUEST",
	
		[[
		]],
		
		to = aliases'X:\\������ ����������\\%(console_name).req', 	-- request address
		message = 'CHECKPASSWORD\r\n%(console_name)\r\n%(user_name)\r\n%(password)\r\nEND',			-- request message template
		from = aliases'X:\\������ ����������\\%(console_name).ok',	-- response address
		timeout = 1000,
		
		decoder=function( args, rsobj )
			if not rsobj then
				return { lasterror = 2, 	warning = 'cannot login user - invalid account or server not responding'}
			end
			local lines = rsobj
			return ( #(lines[1]) > 0 ) and { usergroup= lines[1] } or { lasterror = 1,  warning = not status and  'user logged, but server cannot read user rights' }
		end
	}
	-- end of user logon process and server negotiation --
	
	
	
	-- extract current path
	local app_path = (arg and arg[0] or ''):gsub('[/\\]?[^/\\]+$', '') -- remove file name
	if app_path == '' then app_path = '.' end
	
	Request{ "GET_CONSOLE_CFG_REQUEST", 
	
		[[
		]],
		
		encoder = function(args)
			-- create here actual values for %() placeholders in Transport fields:
			args['~app_path'] = app_path
			args.console_uid = args.console_uid and tostring(args.console_uid)..'' or '�����'
		end,
	
		from = aliases'%(~app_path)\\���������\\%(console_uid).stp',
		
		decoder=function( args, rsobj )
		
			local default_cfg = {
				'��� ���������� ����������� ��������',
				'��� ������ ���������',
				'0', -- bool2str(false)
				'-1', --bool2str(true) 
				10			
			}
			
			local function switchtable( switch, index, values )
				return switch and values[ index ] or default_cfg[ index ]
			end		
		
			local function str2bool( s ) return s~='0' end
			
			local lines = rsobj --[[and tolines( rsobj )]] or {}
			
			if 	#lines > 4 then
				return {
					controller_name	= switchtable( status, 1, lines ),
					console_name		= switchtable( status, 2, lines ),
					autocall				= str2bool( switchtable( status, 3, lines ) ),
					timer_on				= str2bool( switchtable( status, 4, lines ) ),
					timer_tick				= switchtable( status, 5, lines )		
				}
			else
				return {
					lasterror = -1,
					warning = 'cannot load console settings, default values used',
				}
			end
		end
	}-- get_console_cfg (Main.pas - line 277) - ????
	
	
	Command{"SET_CONSOLE_CFG_COMMAND", 
	
		encoder=function(args)
			-- bool2str:
			args.autocall = bool2str( args.autocall )
			args.timer_on = bool2str( args.timer_on )	
			
			-- create here actual values for %() placeholders in Transport fields:
			args['~app_path'] = app_path
			args.console_uid = args.console_uid and tostring(args.console_uid)..'' or '�����'
		end,
		
		message='%(controller_name)\r\n%(console_name)\r\n%(autocall)\r\n%(timer_on)\r\n%(timer_tick)',

		to =  aliases'%(~app_path)\\���������\\%(console_uid).stp'
	}-- set_console_cfg (Config.pas)
	
	----------------------------------------------------------------------
	
	-- console startup 
	-- console_startup_notification (Main.pas - line 345)
	Notification{ "CONSOLE_STARTUP_NOTIFICATION", 
		to = aliases'X:\\������ ����������\\%(console_name).req', 	-- request address
		message = 'START\r\n%(console_name)\r\n%(user_name)\r\nEND',			-- request message template
	}
	
	-- console exit
	-- console_shutdown_notification (Main.pas - line 465 - 480)
	Notification{ "CONSOLE_SHUTDOWN_NOTIFICATION", 
		to = aliases'X:\\������ ����������\\%(console_name).req', 	-- request address
		message = 'STOP\r\n%(console_name)\r\n%(user_name)\r\nEND',			-- request message template
	}
	
	--------------------------------------------------------------------
	
	
	-- build operations list
	--[[
	traversing:
	
	
	-- This code works for me: 
local dircmd = "find . -type f -print" -- default to Unix 
if isWindows then 
	-- Windows 
	dircmd = "dir /b/s" 
else
	-- default to Unix
	dircmd = "find . -type f -print" 
end 

os.execute(dircmd .. " > zzfiles") 

local luafiles = {} 
for f in io.lines("zzfiles") do 
        if f:sub(-4) == ".lua" then 
                luafiles[#luafiles+1] = f 
        end 
end 

print(table.concat(luafiles, "\n")) 
	
	]]
	
		
	--[[
		function (vartable) end
	]]
	
do -- "enum_dir_files" implementation

	local isWindows = string.sub(package.config,1,1) == '\\'
	
	if not isWindows then error('flist enumeration supported now only on WIN32 platform') end
	
	local function split( s, delimiter )
		s = s or ''
		assert( delimiter and #delimiter==1, 'Length of delimiter mus be exactly 1!' )
		local items, pattern = {}, '[^%' .. delimiter .. ']+' --  if delimiter is in magic char(s), escape it by '%'
		for k in string.gmatch( s, pattern ) do table.insert(items, k) end
		return items
	end
	-- function	

	local status, ffi = pcall( require, "ffi" )
	
	if not ffi then -- current lfs dll needs liblua.dll - not compatible with LuaJIT:
		local status, lfs = pcall( require, 'lfs' )
		
		if status then -- lfs module found
		
			function enum_dir_files( path, ext )
				local list={}
				-- code borrowed from example of "lfs": http://keplerproject.github.io/luafilesystem/examples.html
				for file in lfs.dir(path) do -- plain list of directory files, ignore '.', '..' entries, ignore subdirs...
					if file ~= "." and file ~= ".." then
						local f = path..'/'..file
						--print ("\t "..f)
						local attr = lfs.attributes (f)
						assert (type(attr) == "table")
						if attr.mode ~= "directory" then
							local fname, fext = unpack (split( file, '.' ))
							if fext==ext then
								table.insert( list, fname )
								if DUMP then print(fname) end
							end
						end
					end
				end	
				return list				
			end 
			-- function
			
		end	
	end
	
	if enum_dir_files==nil then -- use alternative function implementation
	
		function enum_dir_files( path, ext )
		
			local dircmd = "find . -type f -print" -- default to Unix 
--~ 			local codepagecmd
			
			if isWindows then -- Windows
				dircmd = ("%s\\enumfiles %s %s"):format( app_path, path, 'tmpflist' ) 
			else -- default to Unix
				dircmd = "find . -type f -print" 
			end 

			os.execute(dircmd) 

			local list = {} 
			for file in io.lines("tmpflist") do 
				if file ~= "." and file ~= ".." then
					local fname, fext = unpack (split( file, '.' ))
					if fext == ext then 
						table.insert( list, fname ) 
						if DUMP then print(fname) end
					end 
				end
			end 
			
			return list
		
		end
		-- function
		
	end
	
end

	
	
	Request{ "ENUM_AVAILABLE_OPERATIONS_REQUEST", 
		-- < find solution - directory traversing !!!!!!!!!!!!!!!!!!!!!!>
		from='', -- nothing to return, directory traversing will be performed in "decode" handler
		decoder=function(  )
			
			local path = assert( aliases'X:\\��������', 'Alias cannot be empty' )
			
			
			
			local status, data = pcall( enum_dir_files, path, 'opr' )
			if status then
				return data
			else
				return {
					reason = -1,
					warning = 'cannot enumerate files in direcory: '..data
				}
			end
			
		end
		
	} -- enum_operations_query (Main.pas - line 358 - 377)
	-- X:\��������\\*.opr
	-- ------------------------------------------------------------------
	-- handlers and atomic operations
	
	
	Notification{ "SESSION_RESUME_NOTIFICATION",
		to = aliases'X:\\������ ����������\\%(console_name).req', 	-- request address
		message = 'BACK\r\n%(console_name)\r\n%(user_name)\r\nEND',			-- request message template
	}-- console_resume_notification (Main.pas - lines 519 - 562)
	
	
	Notification{"SESSION_STANDBY_NOTIFICATION",
		to = aliases'X:\\������ ����������\\%(console_name).req', 	-- request address
		message = 'AFK\r\n%(console_name)\r\n%(user_name)\r\nEND',			-- request message template
	}-- console_standby_notification (Main.pas - lines 519 - 562)

	
-- CONSOLE_HEARTBEAT_NOTIFICATION **************************************************************
	
	
	-- note: instead of getcpuclick, use(s) internal pseudo-random generator 0..maxint64
	-- @REQUEST: console_name
	-- @RESPONSE: none
	Notification{ "CONSOLE_HEARTBEAT_NOTIFICATION",
		to = aliases'X:\\������ ����������\\%(console_name).id', 	-- request address
		message='%(~cputick)',
		encoder = function(args) 
			require 'math'
			args['~cputick'] = tostring( math.random( 10000 ) ) -- int64 to string
		end
	}-- console_heartbeat_notification (Main.pas - lines 789 - 800) - usually each 5 sec... - (REFACTOR!)
	

-- ENUM_QUEUE_REQUEST **************************************************************

	-- enum_query_request (Main.pas - lines 870-902)
	
	Request{ "ENUM_QUEUE_REQUEST",
	
		from=aliases'X:\\������ ����������\\%(console_name).que',
		flush_rsbuffer = true,
		
		decoder=function ( args, rsobj )
		
			local function enumtokens( s ) -- extract tokens between square bracket(s) '[...]'
				local items = {}
				for k in string.gmatch( s, '%[([^%[%]]-)%]' ) do table.insert(items, k)  end
				return items
			end

			local lines = rsobj --[[and tolines( rsobj )]] or {}
			-- validate:
			local status = 	lines[1] == 'QUEUE' and lines[#lines] == 'END'
			
			-- exit if error
			if not status then
				return {
					reason=1, 
					warning='Cannot enum queue - data corrupted', 
				}
			end
			
			local data = {}
			
			-- delete messsage envelope string (some(thing) like 'BEGIN', 'END')
			table.remove( lines, 1 ) ;  table.remove( lines --[[, #lines - implicit ]] ) 	
			
			data.queue={} -- list of list~s: list of lines { list of tocken~s }. Note: requested operations item is a list, so data.queue[i] = {date, client_num, is_booked, <rq_basket_len is ignored>, {rq_basket}, <wait time>, <client name>, <client SSN>}
			data.aggregation={} -- requested operation(s) aggregation, format: {'op name':'qty of occurence'}
			-- data.headers = {} -- legend for 
			-- AGREGATE operations in queue (Main.pas - lines 1005 - 1023)
			-- enum and countg request(ed) operations:
			local function pop( t, pos ) return table.remove( t, pos or 1 ) end
			local function append( queue, item ) if item then table.insert( queue, item  ) end end
			
			-- PARSE queue lines (Main.pas - lines 1080 - 1105)
			for _index, _line in ipairs( lines ) do
				-- local _op_in_line = {}
				local _tokens = enumtokens( _line )
				-- append parse~d line to data
				local itemdata={}
				
				--[[1]] append( itemdata, pop( _tokens ) ) -- #date/time
				--[[2]] append( itemdata, pop( _tokens ) ) -- #client number
				--[[3]] append( itemdata, pop( _tokens ) ) -- #is_booked (preliminary registration)
				--[[4]] local _basketlen = pop( _tokens ) or 0 -- quantity of request~ed operation~s, do not append it to process~ed queue list
				--[[5..]] -- opeartion~s:
				
				local itembasket = {}
				local ifirst, ilast = 1, _basketlen -- walk thru 5 token to number of tokens specified in 4-th token
				for j=ifirst, ilast do -- walk thru operations sequence in line: [...]...[4]...[4+n]
					local _operation = pop( _tokens )  -- #operation
					if _operation then
						append( itembasket, _operation )
						local prevcount = data.aggregation[ _operation ] or 0
						data.aggregation[ _operation ] = prevcount + 1
					end -- if _operation then
				end -- for j=ifirst, ilast do
				
				append( itemdata, itembasket )
				
				-- process next token~s if any:
				--[[6..]] --:
				while #_tokens > 0 do
					append( itemdata, pop( _tokens ) ) -- # any add~itional field~s
				end
				
				table.insert( data.queue, itemdata ) -- add process~ed line to queue list
				
			end -- for _index, _line in ipairs( lines ) do
			
			return data
			
		end
	}
	
-- extract_query_item_info_request **************************************************************
	
	
	Request{ "EXTRACT_QUERY_ITEM_INFO_REQUEST",
	
		from=aliases'X:\\������ ����������\\%(console_name).clt',
		
		decoder=function (args, rsobj)

		local lines = rsobj --[[and tolines( rsobj )]] or {}
		local status = 	#lines >= 2 and lines[#lines] == 'END'
		-- delete messsage envelope string (some(thing) like 'BEGIN', 'END')
		if status then 	table.remove( lines, 1 ); table.remove( lines --[[, #lines - implicit ]] ) end		
		return {
			status = status,
			reason = not status and 1,
			warning = status and nil or 'cannot read query item info',
			data = status and lines
		}
	end
	}-- extract_query_item_info_request (Main.pas - lines 920 - 970)
	
	
--[COMMANDS] ======================================================	


	
	-- "NEXTCLIENT" 
	
	Command{ "DO_CALL_NEXT_ITEM_COMMAND",
	
		to = aliases'X:\\������ ����������\\%(console_name).req',
		message = 'NEXTCLIENT\r\n%(console_name)\r\n%(user_name)\r\nEND'
		
	}-- origin <<< (Main.pas - lines 1216..., case 101)
	
	-- "DELETECLIENT" 
	
	Command{ "DO_REMOVE_ITEM_COMMAND",
	
		to = aliases'X:\\������ ����������\\%(console_name).req',
		message = 'DELETECLIENT\r\n%(console_name)\r\n%(user_name)\r\nEND'
		
	}-- origin <<< (Main.pas - lines 1241..., case 102)
	
	-- "NOCLIENT" 
	
	Command{ "DO_PROMPT_IGNORED_COMMAND",
	
		to = aliases'X:\\������ ����������\\%(console_name).req',
		message = 'NOCLIENT\r\n%(console_name)\r\n%(user_name)\r\nEND'
		
	}-- origin <<< (Main.pas - lines 1279..., case 103)
	
	-- "STARTSERV" 
	
	Command{ "DO_ITEM_PROCESSING_BEGIN_COMMAND",
	
		to = aliases'X:\\������ ����������\\%(console_name).req',
		message = 'STARTSERV\r\n%(console_name)\r\n%(user_name)\r\nEND'
		
	}-- origin <<< (Main.pas - lines 1357..., case 104)
	
	-- "FINISHSERV" 
	
	Command{ "DO_ITEM_PROCESSING_END_COMMAND",
	
		to = aliases'X:\\������ ����������\\%(console_name).req',
		message = 'FINISHSERV\r\n%(console_name)\r\n%(user_name)\r\nEND'
		
	}-- origin <<< (Main.pas - lines 1319..., case 104)
		
	-- "CALLFROMNUM" 
	
	Command{ "DO_CALL_ITEM_BY_ID_COMMAND",
	
		to = aliases'X:\\������ ����������\\%(console_name).req',
		message = 'CALLFROMNUM\r\n%(console_name)\r\n%(user_name)\r\n%(item_id)\r\nEND'-- NOTE: item_id is same as in second field in queue list line
		
	} -- origin <<< (Main.pas - lines 1384..., case 105)
		
	-- "MOVECLIENT" 
	
	Command{ "DO_DEFER_ITEM_COMMAND",
	
		to = aliases'X:\\������ ����������\\%(console_name).req',
		message = 'MOVECLIENT\r\n%(console_name)\r\n%(user_name)\r\n%(delay)\r\nEND'
		
	} -- origin <<< (Main.pas - lines 1476..., case 106)
		
	-- "SKIP" 
	
	Command{ "DO_ITEM_BYPASS_COMMAND",
	
		to = aliases'X:\\������ ����������\\%(console_name).req',
		message = 'SKIP\r\n%(console_name)\r\n%(user_name)\r\nEND'
		
	}-- origin <<< (Main.pas - lines 1510..., case 107)
		
	-- "CHANGE_OPER" 
	
	Command{ "DO_REDIRECT_ITEM_COMMAND",
	
		to = aliases'X:\\������ ����������\\%(console_name).req',
		message = 'CHANGE_OPER\r\n%(console_name)\r\n%(user_name)\r\n%(tag)\r\nEND'
		
	}-- origin <<< (Main.pas - lines 1544..., case 190)
	

--~ print( enumServicesInfo() )	
---------------8X-------------------
