
-- response class:
response={}

require 'request' -- uses request.location.resolve()

-- utils
    function escape(s)
        s = string.gsub(s, "([&=+%c])", function(c) return string.format("%%%02X", string.byte(c)) end)
        s = string.gsub(s, " ", "+")
        return s
    end

function response.reset()
    response.statecode=200
    response.headers={} -- table (keyed) for headers
    response.headers['Content-Type']='text/html'
    response.headers['Connection'] = 'Close'
    response.body={} -- table (list) for string storage
end

function response.status(code, headers)
    response.statecode = code
    if headers then
        for name, value in pairs(headers) do
            response.headers[name] = value
        end
    end
end

function response.notfound(errmessage) 
    response.status(403)
    response.write (errmessage)
end

function response.notallowed(errmessage) 
    response.status(405)
    response.write (errmessage)
end

function response.servererror(errmessage)
    response.status(500)
    response.write(errmessage)
end

function response.redirect(path)
    response.status( 302, {Location=request.location.resolve(path)} )
end

function response.write(s)
    if not s then return end -- avoid empty strings that can break down correct length definition
    if type(s)=='table' then 
        for i=1,#s do response.write(s[i]) end
    else
        response.body[#response.body+1]=tostring(s)
    end
end

function response.lines()
    local index=0
    local iterator= function()
        index = index + 1
        return response.body[index]
    end
    return iterator, response.body, 0
end

--~ function response.render(s, template)
--~     if type(s)=='table' and not template then error('response.render is not implemented for generic template') end
--~     local text = (type(s)=='string') and s or sformat( template, s)
--~     response.body:set( text )
--~ end

function response.flush( outmethod ) -- trick: to use templated output, make separate lua module that redefines response.flush(), attach this module from 'app.lua' via 'require'
    outmethod = outmethod or io.write
    outmethod(string.format('HTTP/1.1: %s\r\n', response.statecode))
    for name, value in pairs( response.headers ) do
        outmethod( string.format('%s: %s\r\n', name, value) )
    end
    outmethod('\r\n\r\n\n')
    for line in response.lines() do
        outmethod( line )
    end
end

local mt = {}
setmetatable( response,  mt)
mt.__call = function(self, s) self.write(s) end


response.reset()

return response
