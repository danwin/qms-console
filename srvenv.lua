--[[srvenv]]
-- former "dispatch"
--[[
    server environment and utility functions
    implements CGI gateway
]]
srvenv = {}

    if __DEBUG__ then 
        io.stderr=io.stdout
        local syserror = error
        function error(msg)
            print(msg)
            syserror(msg)
        end
    end

    -- global request static object
    request = require 'request'
    -- global response static object
    response = require 'response'
    -- global output method, can be redefined later from application(e.g. for templating)
    renderer = io.write 

    function srvenv.mountmodule( appfname)
        local status, result = pcall(require, appfname)
        if status then
            return result
        else
            response.servererror ("Cannot load main application: " .. result )
            response.flush(renderer)
            os.exit()
        end  
    end

    -- global function to process cgi request
    function srvenv.run( mainproc ) -- application main proc or callable object, accepts parameters request, response, can return string value or use response output method(s)
        if mainproc then
            local status, message, httpcode, httpheaders = pcall(mainproc, request, response)
            if status then 
                if httpcode then response.status( httpcode, httpheaders ) end
                if message then -- if application returns response fields directly, instead of calling "response" methods
                    if type(message)=='string' then
                        response.write(message)
                    else 
                        response.servererror('Invalid response format: '..type(message))
                    end
                end
            else
                response.servererror(message)
            end        
        else -- mainproc ".execute" not found
            response.servererror('mainproc( request, response ) was not specified')
        end
        response.flush(renderer)
        os.exit()
    end

return srvenv