-- html helpers

local helpers={}

	function helpers.SELECT(list)
		local result={}
--~ 		list=list or {}
		for _, item in ipairs( list) do
			table.insert( result,  '<option value=\"'..item..'\">'..item..'</option>')
		end
--~ 		for _, item in pairs( list) do
--~ 			item = tostring(item)
--~ 			table.insert( result,  '<option value=\"'..item..'\">'..item..'</option>')
--~ 		end
		return table.concat(result, '\r')
	end	
	
	function helpers.ERROR( result ) -- reports error in DIV element with class "error"
		return '<div class=\"error\">Errorcode: '..tostring(result.errcode or '-')..'; Warning: '..tostring(result.warning or '-')..'</div>'  
	end


return helpers