--[[utils.lua]]

utils={}

    utils.uri={}
    
    function utils.uri.ltrimslash( path )
        path = (path or ''):gsub( '^/', '')
        return path
    end
    
    function utils.uri.rtrimslash( path )
        path = (path or ''):gsub( '/$', '')
        return path
    end 

    function utils.uri.trimslash( path )
        path = (path or ''):gsub('^/', ''):gsub('/$', '') --
        return path
    end
    
    function utils.uri.split( path, delimiter )
        local items = {}
        delimiter = delimiter or '/'
        
--~         print('Splitting: '..type(path).. '=' ..(path or 'None')..'   '..type(delimiter)..'='..(delimiter or 'None'))
        
        assert( #delimiter==1, 'Length of delimiter mus be exactly 1!' )
        local pattern = '[^%' .. delimiter .. ']+' -- note for % escape char - important if delimiter is in magic char(s): ^$()%.[]*+-?
--        for k in string.gmatch( path, '[^/]+' ) do
        for k in string.gmatch( path, pattern ) do
            table.insert(items, k)
        end
        return items
    end

    --[[escape, unescape ]] 

    function utils.escape(s)
        s = string.gsub(s, "([&=+%c])", function(c) return string.format("%%%02X", string.byte(c)) end)
        s = string.gsub(s, " ", "+")
        return s
    end

    function utils.unescape(s)
        s = string.gsub(s, "+", " ") 
        s = string.gsub(s, "&amp;", "&")
        s = string.gsub(s, "&lt;", "<")
        s = string.gsub(s, "&gt;", ">")   
        return string.gsub(s, "%%(%x%x)", function(hex)
            return string.char(base.tonumber(hex, 16))
        end)
    end
    
    -- decode_fields
    function utils.decode_fields( query )

        local fields = {}
        for key, value in string.gmatch( query, "([^=&]+)=([^=&]+)" ) do --"(%w+)=(%w+)"
            if key and value then
                fields[key] = utils.unescape(value)
            end
        end --for   
        return fields     
    end
    

return utils