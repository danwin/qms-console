--[[svcwrapper.lua]]
-- this is application specific file, not from mcgi core!
--    Utils to create wrapped services

--[[
	TO-DO:
	2. cleanup~s when necessary - important - see USER_LOGIN_REQUEST !!!!!!!!!!!!!!!!!!!!!!!!
	3. JS stubs
]]
DUMP=false

--[[ FOR FUTURE - protected calls:
http://lua-users.org/wiki/FinalizedExceptions

Using finalized exceptions

or How to get rid of all those if statements

by DiegoNehab

Abstract

This little LTN describes a simple exception scheme that greatly simplifies error checking in Lua programs. All the needed functionality ships standard with Lua, but is hidden between the assert and pcall functions. To make it more evident, we stick to a convenient standard (you probably already use anyways) for Lua function return values, and define two very simple helper functions (either in C or in Lua itself).
Introduction

Most Lua functions return nil in case of error, followed by a message describing the error. If you don't use this convention, you probably have good reasons. Hopefully, after reading on, you will realize your reasons are not good enough.

If you are like me, you hate error checking. Most nice little code snippets that look beautiful when you first write them lose some of their charm when you add all that error checking code. Yet, error checking is as important as the rest of the code. How sad.

Even if you stick to a return convention, any complex task which involving several function calls makes error checking both boring and error-prone (do you see the error below?)


function task(arg1, arg2, ...)
    local ret1, err = task1(arg1)
    if not ret1 then 
        cleanup1()
        return nil, err 
    end
    local ret2, err = task2(arg2)
    if not ret then 
        cleanup2()
        return nil, err 
    end
    ...
end
The standard assert function provides an interesting alternative. To use it, simply nest every function call to be error checked with a call to assert. The assert function checks the value of its first argument. If it is nil, assert throws the second argument as an error message. Otherwise, assert lets all arguments through as if had not been there. The idea greatly simplifies error checking:

function task(arg1, arg2, ...)
    local ret1 = assert(task1(arg1))
    local ret2 = assert(task2(arg2))
    ...
end
If any task fails, the execution is aborted by assert and the error message is displayed to the user as the cause of the problem. If no error happens, the task completes as before. There isn't a single if statement and this is great. However, there are some problems with the idea.

First, the topmost task function doesn't respect the protocol followed by the lower-level tasks: It raises an error instead of returning nil followed by the error messages. Here is where the standard pcall comes in handy.

function xtask(arg1, arg2, ...)
    local ret1 = assert(task1(arg1))
    local ret2 = assert(task2(arg2))
    ...
end

function task(arg1, arg2, ...)
    local ok, ret_or_err = pcall(xtask, arg1, arg2, ...)
    if ok then return ret_or_err
    else return nil, ret_or_err end
end
Our new task function is well behaved. Pcall catches any error raised by the calls to assert and returns it after the status code. That way, errors don't get propagated to the user of the high level task function.

These are the main ideas for our exception scheme, but there are still a few glitches to fix:

Directly using pcall ruined the simplicity of the code;
What happened to the cleanup function calls? What if we have to, say, close a file?
Assert messes with the error message before raising the error (it adds line number information).
Fortunately, all these problems are very easy to solve and that's what we do in the following sections.

Introducing the protect factory

We used the pcall function to shield the user from errors that could be raised by the underlying implementation. Instead of directly using pcall (and thus duplicating code) every time we prefer a factory that does the same job:

local function pack(ok, ...)
    return ok, arg
end

function protect(f)
    return function(...)
        local ok, ret = pack(pcall(f, unpack(arg)))
        if ok then return unpack(ret)
        else return nil, ret[1] end
    end
end
The protect factory receives a function that might raise exceptions and returns a function that respects our return value convention. Now we can rewrite the top-level task function in a much cleaner way:

task = protect(function(arg1, arg2, ...)
    local ret1 = assert(task1(arg1))
    local ret2 = assert(task2(arg2))
    ...
end)
The Lua implementation of the protect factory suffers with the creation of tables to hold multiple arguments and return values. It is possible (and easy) to implement the same function in C, without any table creation.

static int safecall(lua_State *L) {
    lua_pushvalue(L, lua_upvalueindex(1));
    lua_insert(L, 1);
    if (lua_pcall(L, lua_gettop(L) - 1, LUA_MULTRET, 0) != 0) {
        lua_pushnil(L);
        lua_insert(L, 1);
        return 2;
    } else return lua_gettop(L);
}

static int protect(lua_State *L) {
    lua_pushcclosure(L, safecall, 1);
    return 1;
}
As of Lua 5.1 the temporary table can be avoided in pure Lua code, too:

do
    local function fix_return_values(ok, ...)
        if ok then
            return ...
        else
            return nil, (...)
        end
    end

    function protect(f)
        return function(...)
            return fix_return_values(pcall(f, ...))
        end
    end
end
The newtry factory

Let's solve the two remaining issues with a single shot and use a concrete example to illustrate the proposed solution. Suppose you want to write a function to download an HTTP document. You have to connect, send the request and read the reply. Each of these tasks can fail, but if something goes wrong after you connected, you have to close the connection before returning the error message.

get = protect(function(host, path)
    local c
    -- create a try function with a finalizer to close the socket
    local try = newtry(function() 
        if c then c:close() end 
    end)
    -- connect and send request
    c = try(connect(host, 80))
    try(c:send("GET " .. path .. " HTTP/1.0\r\n\r\n"))
    -- get headers
    local h = {}
    while 1 do
        l = try(c:receive())
        if l == "" then break end
        table.insert(h, l)
    end
    -- get body
    local b = try(c:receive("*a"))
    c:close()
    return b, h
end)
The newtry factory returns a function that works just like assert. The differences are that the try function doesn't mess with the error message and it calls an optional finalizer before raising the error. In our example, the finalizer simply closes the socket.

Even with a simple example like this, we see that the finalized exceptions simplified our life. Let's see what we gain in general, not just in this example:

We don't need to declare dummy variables to hold error messages in case any ever shows up;
We avoid using a variable to hold something that could either be a return value or an error message;
We didn't have to use several if statements to check for errors;
If an error happens, we know our finalizer is going to be invoked automatically;
Exceptions get propagated, so we don't repeat these if statements until the error reaches the user.
Try writing the same function without the tricks we used above and you will see that the code gets ugly. Longer sequences of operations with error checking would get even uglier. So let's implement the newtry function in Lua:

function newtry(f)
    return function(...) 
        if not arg[1] then 
            if f then f() end 
            error(arg[2], 0)  
        else 
            return unpack(arg) 
        end
    end
end
Again, the implementation suffers from the creation of tables at each function call, so we prefer the C version:

static int finalize(lua_State *L) {
    if (!lua_toboolean(L, 1)) {
        lua_pushvalue(L, lua_upvalueindex(1));
        lua_pcall(L, 0, 0, 0);
        lua_settop(L, 2);
        lua_error(L);
        return 0;
    } else return lua_gettop(L);
}

static int do_nothing(lua_State *L) {
    (void) L;
    return 0;
}

static int newtry(lua_State *L) {
    lua_settop(L, 1);
    if (lua_isnil(L, 1)) 
        lua_pushcfunction(L, do_nothing);
    lua_pushcclosure(L, finalize, 1);
    return 1;
}
Again, starting with Lua 5.1 an efficient implementation without temporary tables is possible in plain Lua code:

function newtry(f)
    return function(ok, ...)
        if ok then
            return ok, ...
        else
            if f then f() end
            error((...), 0)
        end
    end
end
Final considerations

The protect and newtry functions saved a lot of work in the implementation of LuaSocket[1]. The size of some modules was cut in half by the these ideas. It's true the scheme is not as generic as the exception mechanism of programming languages like C++ or Java, but the power/simplicity ratio is favorable and I hope it serves you as well as it served LuaSocket.


]]


services={}

-- utils =================================================================
	local function sformat(args, s, ignoremissed)
		if args~=nil then
			if type(args)~='table' then error('Invalid sformat call: args must be a table!') end
			local template, mask = s,  "%%%(([%w_%~]+)%)" -- work with %(name) placeholders, note that only alphanumeric chars and underscore allowed
			local result= string.gsub(template, mask, args)
			if not ignoremissed and result:match( mask ) then error('Template error - some values are absent: '..result:match( mask ) ) end
			--replace empty placeholders if any:
			result= string.gsub(result, mask, "")
			return result
		end
		return s
	end
	
--~ 	local function tostrings( fileobj )
--~ 		local result = {}
--~ 		for line in fileobj:lines() do -- result of receive and request is file-like object, alternative is to use :read('*a') method and try: contents:match('(.-)\r\n')
--~             if DUMP then  print('line::: '..line..'\r\n') end
--~ 			table.insert(result, line)
--~ 		end
--~ 		return result
--~ 	end

	local function tostrings( s )
        assert( type(s)=="table" )
        return s
--~ 		local result = {}
--~ 		for line in s:gmatch('([^\n\r?]*)') do -- result of receive and request is file-like object, alternative is to use :read('*a') method and try: contents:match('(.-)\r\n')
--~             --[[TMP]] print('line::: '..line..'\r\n')
--~ 			table.insert(result, line or '')
--~ 		end
--~ 		return result
	end
    
    local function toboolean( v )
        return v and true or false
    end

--~ 	local writefile = function(fname, contents) io:write('Writing file: \"'..fname..'\"'..contents) end   -- debug version
--    local writefile = function(fname, contents) return assert(io.open(fname, "w")):write(contents) end 
	
	local sleep -- sleep( msectimeout ) -- see JIT FFI receipts
	do -- define
		local status, ffi = pcall( require, 'ffi')
		if status then -- JIT FFI module load~ed
        
            ffi.cdef[[
            void Sleep(int ms);
            int poll(struct pollfd *fds, unsigned long nfds, int timeout);
            ]]

            if ffi.os == "Windows" then
              function sleep(s)
                ffi.C.Sleep(s)
              end
            else
              function sleep(s)
                ffi.C.poll(nil, 0, s)
              end
            end
            
        end
        
        if not sleep then -- redundant definition
		  function sleep(s)
            s = s or 1000
            s = s / 1000 -- ( input in milliseconds )
			local start=os.time()
            while os.difftime( os.time() - start ) < s do
                --
            end
		  end            
        end
	end	-- define
	
-- Transport object~s ====================================================
	-- trick below - see "closure" code from http://lua-users.org/wiki/ObjectOrientationClosureApproach+ my own idea how simplify code if no class attr(s) necessity
	
	function Transport(attr)
	
		--[[
		try the next:
		local x=0
		self.setX( value ) x=value end
		self.setX( attr.X )
		
		local s -- you do not have to init! definition is sufficient
		self.setS( value ) s = value and value end
		self.setS( attr.s )
		
		]]
		
		local self={}
		
		local Ctx = assert( attr.Ctx, 'Transport context argument cannot be undefined!' )
		
		local function preprocess_argument( a ) -- Note! RQA, RSA, RQM - can be "plain" string, template string with named placeholders like '%(argname)', will be called in context of Ctx
			return ( type( a )=='string' ) and sformat( Ctx, a ) or a -- if any %(name) placeholders - replace them
--~ 			local result = (type(a)=='function')  and a(Ctx)  or a -- if argument is callable - call it :) it's encoder function
--~ 			return sformat( Ctx, a )
		end
		
		local RQA -- (private) request address (can be template, placeholders later will be replace(d) by Ctx fields)
		function self.setRQA ( s ) 
			RQA = s and preprocess_argument( s ) 
		end
		self.setRQA( attr.RQA ) -- init if value specified
		
		local RSA -- (private) response address (can be template, placeholders later will be replace(d) by Ctx fields)
		function self.setRSA ( s ) 
			RSA = s and preprocess_argument( s )	
		end
		self.setRSA(attr.RSA) -- init if value specified
		
		local RQM -- (private) request message (can be encoder function (return~s string or string template), plain string or template, in whi~ch placeholders later will be replace(d) by Ctx fields)
		function self.setRQM ( s )
			RQM = s and preprocess_argument( s )	
		end
		self.setRQM( attr.RQM ) -- init if value specified
		
		local timeout -- (private) timeout between send and receive
		function self.setTimeout ( msec )	
			timeout = msec or 1000 
		end
		self.setTimeout(attr.timeout) -- init if value specified
		
		self.cleanup_request = attr.cleanup_request~=nil or false
		self.cleanup_response = attr.cleanup_response~=nil or false		
		
		self.dosend = nil -- perform operation or throw exception
		self.doreceive = nil -- return file-like object or throws exception
		self.doflush = nil -- perform operation or throw exception
		
		function self.send(  ) 
			return self.dosend( RQA, RQM ) 
		end
		function self.receive(  ) -- must return file-like iterator object with :lines(), :write() and :read() protocol
			return self.doreceive( RSA ) 
		end 
		function self.checkresponse( _RSA ) -- check(s) that response exists, return(s) logical value (nil or non-empty)
			local tmpRSA = _RSA and sformat( Ctx, _RSA) or RSA 
			return toboolean( self.doreceive( tmpRSA ) )
		end
		
		function self.wait( msec )
			sleep( msec or timeout )
		end
		
		function self.flush_rqbuffer()
			local status = pcall(self.doflush, RQA )
			return status
		end
		
		function self.flush_rsbuffer()
			local status = pcall(self.doflush, RSA )
			return status
		end
		
		function self.request( ) -- the whole cycle of response .. wait .. request
			if self.cleanup_request then self.flush_rqbuffer()  end
			if self.cleanup_response then self.flush_rsbuffer() end
			self.send( )
			self.wait( )
            return self.receive() -- do not loss optional second result - error message
--~ 			local result = self.receive()
--~ 			return result			
		end
		
		return self
	end
	
	require 'os' -- system-dependent file utils
	
	function FileTransport( attr )
		local self = Transport( attr ) -- parent class
		
		-- implementation of file-specific methods:
		function self.dosend(endpoint, message)
            local f=assert(io.open(endpoint, "w"))
			f:write( message )
            f:close()
            return true
		end
		
		function self.doreceive( endpoint ) -- return file-like object with methods - :read('*a'); :lines()
--~ 			return assert( io.open( endpoint ) ):read('*a')
--~             --[[TMP]] print('Opening file: '..endpoint)
--~             local f=assert( io.open( endpoint, 'rb' ) )
--~             local buffer=f:read('*a')
--~              --[[TMP]] print('FILE: '..buffer )
--~             f:close()
--~             do return buffer end

            local result={}
            if DUMP then  print('Opening file: '..endpoint) end
            local f=assert( io.open( endpoint--[[, 'rb']] ), ('Error opening remote resource: \"%s\"'):format(endpoint) )
            for buffer in f:lines() do
                buffer=buffer:gsub('\n',''):gsub('\r','') -- remove any occurense of \r \n from string
                if DUMP then  print('FILE: [['..buffer..']]' ) end
                table.insert(result, buffer)
            end
            f:close()
            do return result end

--~             
--~             
--~             --[[TMP]] print('FILE: '..assert( io.open( endpoint ) ):read('*a') )
--~             
--~             for line in assert( io.open( endpoint ) ):lines() do print('*** '..line); table.insert( result, line ) end
--~             
--~             --[[TMP]] print('Done file'..endpoint)
--~             
--~             return result
		end
		
		function self.doflush( dest )
			assert( os.remove( dest ) )
		end
		
		return self
	end
	
	
-- Shortcuts - utils to create conversation~s ============================================================

--~ 	function RemoteService( ... )
--~ 		local settings, params = ...
--~ 		assert( params~=nil, 'remote service params not specified' )
--~ 		self.settings={
--~ 			transport = 
--~ 		}
--~ 		local self={}
--~ 		
--~ 		return  self
--~ 	end
	
	local service_registry={
	}
	--[[
	-- fields:
	
		"name"={
			handler = srv_handler,
			args = srv_args,
			_returns = srv_returns,
			description = srv_description
		},
		...
	]]
	

	
	local function extractAttrs( attrs )
		local sa = {}
		-- extract args names from string template~s ( names stored in %(name) placeholders, if any )
		for i=1,table.maxn( attrs ) do
			if type( attrs[i] )=='string' then
				local template, mask = attrs[i],  "%%%(([%w_]+)%)" -- work with %(name) placeholders, note that only alphanumeric chars and underscore allowed
				for varname in string.gmatch(template, mask, attrs) do
					sa[varname]=true -- later store here arg type, etc.,,,
				end
			end
		end
		return sa
	end
	
	function RegisterService( regdata ) 
		assert( not service_registry[regdata.name], (('cannot register service \"%s\" - this name already used'):format( regdata.name)  ) )
		-- extract args from template here:
		local attrs = extractAttrs( regdata.attrs )
		service_registry[regdata.name] = {
			handler = regdata.handler,
			attrs = attrs,
			description = regdata.description
		}
	end
	
	-- convert~s result to table structure (json-compatible), with mandatory "status" attribute
	local function uniformResult( handler, args, servicename )

		
		local function forcetypecast( v )
			local function processtable( t )
				for i, val in ipairs(t) do t[i] = forcetypecast( val ) end -- treat table as list
				for key, val in pairs(t) do t[key] = forcetypecast( val ) end -- treat table as hash
				return t
			end

			if type(v) == 'table' then return processtable(v) end -- recursion
			if tonumber(v) then return  tonumber(v) end
			if v == "true" then return  true end
			if v == "false" then return  false end
            if type(v)=='string' then return v end
			return v
		end		

		local opstate, data = pcall( handler, args ) -- note - handler also return~s 2 values - <boolean>opstate and result or error message 
		if DUMP then  print( 'handler called, pcall result: '..tostring(opstate)..'; result type: '..type(data)..'\r\n' ) end
        
		-- combine status:
		opstate = opstate and ( not (type(data) =='table' and data.lasterror~=nil) )-- if any

		local warning, errcode = nil, nil        
        if not opstate then
            warning = ( type(data)=='string' and data ) or ( type(data)=='table' and data.warning ) or 'error during request: '..tostring (data or '') 
            errcode = (type(data)=='table' and data.lasterror)  or  100
        end		
		
		return {
			ok=opstate,
			warning=warning,
			errcode=errcode,
			result=opstate and forcetypecast( data ) or nil,
			
			service=servicename
		}
				
	end	
	
	
    local function findServiceReg( name ) -- return service registry record {handler, attrs, description}
        local regdata = assert( name and service_registry[name], ('service \"%s\" is undefined'):format( name or '' ) )
        return regdata
    end
    
	function callService( name, args ) -- args is {}
	--[[
	@throws: 'service \"%s\" is undefined'
	]]
		local handler = assert( findServiceReg(name).handler, ('service \"%s\" handler is undefined'):format( name or '' ) )
		return uniformResult( handler, args, name--[[service name to insert into response]] )
	end
    
    function serviceInfo(name)
        local servicedata = findServiceReg(name)
        local s='Service name: '..name..'\nArguments: '
        for attrname, _ in pairs( servicedata.attrs ) do
            s=s..attrname..';'
        end
        s=s..'\nDescription: '..(servicedata.description or 'none')
        return s
    end
    
    
    
    function enumServices()
        local list={}
        for name, _ in pairs( service_registry ) do table.insert( list, name ) end
        return list
    end
    
    function enumServicesInfo()
        local buffer={}
        for name, _ in pairs( service_registry ) do
            table.insert( buffer,  serviceInfo(name))
        end
        return table.concat( buffer, '\n' )
    end
	

	-- NOTIFICATION: send, not check aknowlegment
	-- calling convention: Notification{ name, to=value, message=value, ... ,<description>}
	function Notification( params )

		-- additional check of arguments:
		if (params.encoder) then assert( type(params.encoder)=='function' or params.encoder.__call ~=nil, 'request encoder must be function or callable object' ) end	
	
		RegisterService{
			name = params[1], -- "positional" params
			
			attrs={assert(params.to), assert(params.message)}, -- these args contains %( ) placeholder
			
			handler=function(args)
				-- PRE#PROCESS arguments, encode it:
				if params.encoder~=nil then assert( pcall(params.encoder, args )) end

				local _ = FileTransport{Ctx=args,
					RQA = params.to, 	-- request end~point
					RQM = params.message			-- request message (possible values: plain string, or  string template with placehoders for Ctx fileds)
				}
				return pcall( _.send ) -- returns multiple values: <boolean>status, <string> error message or nil
			end,
			
			description=params[2] -- "positional" params
		}
	end	
	
	-- COMMAND: send & check aknowlegment
	function Command( params )

			-- additional check of arguments:
		if (params.encoder) then assert( type(params.encoder)=='function' or params.encoder.__call ~=nil, 'request encoder must be function or callable object' ) end
		
		RegisterService{
			name = params[1], -- "positional" params
			
			attrs={assert(params.to), assert(params.message), params.from--[[optional]], params.timeout --[[optional]]}, -- these args contains %( ) placeholder
	
			handler= function(args)
				-- PRE#PROCESS arguments, encode it:
				if params.encoder~=nil then assert( pcall(params.encoder, args )) end

				local _ = FileTransport{Ctx=args,
					RQA = params.to, 	-- request end~point
					RQM = params.message,			-- request message (possible values: plain string, or  string template with placehoders for Ctx fileds)
					RSA = params.from or params.to, -- default - same as RQA (if "from" not specified) only to check exist~anse of server result
					timeout = params.timeout or 1000
				}
				assert( pcall( _.send ) )
				_.wait()
                
                return assert( toboolean( _.checkresponse() ) )
                
--~ 				local opstate, result = pcall(  _.checkresponse )
--~ 				opstate = opstate and toboolean( result ) -- combine status of code errors and operation errors
--~ 				result = not opstate and result or nil
--~ 				return  opstate, result -- return~s multiple values: <boolean>status, <string> error message or nil
			end,
            
--~             _returns=[[
--~             {
--~ 			<boolean> ok: true if no errors during request,
--~ 			<string> warning: contains error message,
--~ 			<int>errcode: numerical code of error,
--~ 			<boolean>result: true if requested resource exists,
--~ 			
--~ 			<string>servicename: name if service invoke~d
--~             }            
--~             ]],
			
			description=params[2] -- "positional" params
		}
	end

	-- REQUEST: send, then read responce and decode it (if necessary)
	function Request( params )

		-- additional check of arguments:
		if (params.encoder) then assert( type(params.encoder)=='function' or params.encoder.__call ~=nil, 'request encoder must be function or callable object' ) end
		-- additional check of arguments:
		if (params.decoder) then assert( type(params.decoder)=='function' or params.decoder.__call ~=nil, 'request decoder must be function or callable object' ) end
	
		RegisterService{
			name = params[1], -- "positional" params
			
			attrs={params.to, params.message --[[optional]], assert(params.from), params.timeout --[[optional]], params.decoder --[[optional]]},
			
			handler= function(args)
				-- PRE#PROCESS arguments, encode it:
				if params.encoder~=nil then assert( pcall(params.encoder, args )) end
				local _ = FileTransport{Ctx=args,
					RQA = params.to, 	-- request end~point
					RQM = params.message,			-- request message (possible values: plain string or string template with placehoders for Ctx fileds, plain string, or  string template with placehoders for Ctx fileds)
					RSA = params.from,	-- response end~point (here is target file to check it exist~ance)
					timeout = params.timeout or 1000
				}
	--~ 			t.flush_rqbuffer() -- remove old request file
				if message then -- if response is not persistent and necessary to send specific message
					assert(pcall(_.send))
					_.wait()			
				end
                
				local rsobj
                if params.from and params.from~='' then rsobj= assert( _.receive()  ) end -- if error, throws multiple values: <boolean>status, <string> error message
                
                return assert( params.decoder ( args,  rsobj ) )
--~ 				
--~ 	--~ 			t.flush_rsbuffer() -- remove old response file	
--~ 				local result
--~ 				if params.decoder~=nil then
--~ 					opstate, result = assert( pcall( params.decoder, args,  rsobj ) ) -- if error, throws multiple values: <boolean>status, <string> error message
--~ 				else
--~ 					result=nil
--~ 				end
--~ 				return true, result
			end,
			
			description=params[2] -- "positional" params
		}
	end	


	jsstubs={}
	
	function linkJSstub( service_name, args )
		jsstubs[ service_name ] = sformat(
		)
	end
	
-- Protocol~s ===================================================================


    
function tabletostring(t, shift)
    shift=shift or ''
    local s=shift..'{\n'
    for i, v in ipairs(t) do if type(v)=='table' then s=s..'\n'..shift..'['..i..']:'..tabletostring(v, shift..'    ') else s=s..'\n'..shift..'['..i..']='..tostring(v) end end
    for k, v in pairs(t) do if type(v)=='table' then s=s..'\n'..shift..'\"'..k..'\":'..tabletostring(v, shift..'    ') else  s=s..'\n'..shift..'\"'..k..'\":'..tostring(v) end end
    return s..';\n'..shift..'};\n'
end

if DUMP and DEBUG then -- test

	Notification{ "TEST_SERVICE_NOTIFICATION",
	
		[["This is a
		test service"
		]],
		
		message='BEGIN\r\nThis is test file: [%(arg1)]\r\n\r\nEND',
		
		to='d:\\tmp\\%(testfile).txt',
		from='d:\\tmp\\%(testfile).txt',
	}
    
	Command{ "TEST_SERVICE_COMMAND",
	
		[["This is a
		test service"
		]],
		
		message='BEGIN\r\nThis is test file: [%(arg1)]\r\n\r\nEND',
		
		to='d:\\tmp\\%(testfile).txt',
		from='d:\\tmp\\%(testfile).txt',
	}

    
    Request{ "TEST_SERVICE_REQUEST",
	
		[["This is a
		test service"
		]],
		
		--message='BEGIN\r\nThis is test file: [%(arg1)]',
		
		--to='d:\\tmp\\%(testfile).txt',
		from='d:\\tmp\\%(testfile).txt',
        
        decoder=function(args, rsobj) 
--~             --[[TMP]]
--~             print('>>>'..rsobj) 
            if DUMP then print('Decoder called\r\nType of rsobj: '..type(rsobj)..'\r\nHas lines(): '..tostring(toboolean(rsobj and rsobj.lines)) ) end
--~             local lines= tostrings(rsobj)
            local lines= rsobj or {}
            local result=''
            if DUMP then  print('Result length: '..#lines..'\r\n') end
--~             print('LINES: '..lines)
            for i=1,#lines do
                result=result..(lines[i] or '')..'\n'
                if DUMP then  print ('['..tostring(i)..'] '..lines[i]) end
            end
            if DUMP then  print('Decoder internal result type: '..type(result)..'\r\n') end
            if DUMP then  print('result is: \"'..result..'\"') end
            return result
        end
	}


--~     print'***************************************'
--~     print( tabletostring( callService( 'TEST_SERVICE_NOTIFICATION', {arg1='��� ����������', testfile='���_����_ntf'} )))
--~     print'***************************************'
--~     print( tabletostring( callService( 'TEST_SERVICE_COMMAND', {arg1=1, testfile='���_����_cmd'} )))
--~     print'***************************************'
--~     print( tabletostring( callService('TEST_SERVICE_REQUEST', {arg1=1, testfile='���_����_ntf'}) ))
--~     print'***************************************'
end

return services

