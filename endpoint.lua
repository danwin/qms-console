#!ljcgi.exe
--[[endpoint]]
--[[ This is main project module ]]

__DEBUG__=true --debug mode, turn off in production

--set path for packages if they are not in the same folder
require('setpath')

server = require 'srvenv'
--[[instead of using 'srvenv' module, you can directly use modules 'request', 'response', depending of necessity]]

server.mountmodule 'main'



