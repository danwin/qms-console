/**
* logging
*/
function _log_(msg) { 

	var currentTime = new Date();
	var hours = currentTime.getHours();
	var minutes = currentTime.getMinutes();
	var seconds = currentTime.getSeconds();
	var milliseconds = currentTime.getMilliseconds();
	minutes = (minutes < 10) ?  "0" + minutes : minutes;
	seconds = (seconds < 10) ?  "0" + seconds : seconds;
	milliseconds = (milliseconds < 10) ?  "00" + milliseconds : ( (milliseconds < 100) ? "0" + milliseconds : milliseconds);
	msg = ++_log_.counter + " ["+hours+":"+minutes+":"+seconds+":"+milliseconds+"] : "+ msg;

	if( window.console && window.console.log ) window.console.log(msg); 
	else {
		var log_display = document.getElementById("logger");
		if (log_display) log_display.innerHTML += msg+"<br>"; //else alert (msg); 
	} 
}
_log_.counter = 0;
/* */