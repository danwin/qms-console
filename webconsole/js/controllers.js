		function getContent($destID, url, beforeSend, onComplete, onError)
		{
			var options = {}
			options["url"] = url || "404.html";
			options["type"] = "GET";
			options["dataType"] = "html"; // type of data expected back from the server (xml, html, json, or script)
			options['contentType']='text/html; charset=utf-8';
			options["timeout"] = 5000;
			options["beforeSend"] = beforeSend || function (){ $($destID).text("���������, ����������...") };
			options["success"] = onComplete || function( data ){ $($destID).html( data ) };
			options["error"] = onError || function( data ){ $($destID).html( '<h1>� ���������, ����������� ���������� � ������ ������ ����������</h1>' ) };
			$.ajax(options);
		}    

		function postContent($destID, url, beforeSend, onComplete, onError)
		{
			var options = {}
			options["url"] = url || "404.html";
			options["type"] = "GET";
			options["dataType"] = "html"; // type of data expected back from the server (xml, html, json, or script)
			options['contentType']='text/html; charset=utf-8';
			options["timeout"] = 5000;
			options["beforeSend"] = beforeSend || function (){ $($destID).text("���������, ����������...") };
			options["success"] = onComplete || function( data ){ $($destID).html( data ) };
			options["error"] = onError || function( data ){ $($destID).html( '<h1>� ���������, ����������� ���������� � ������ ������ ����������</h1>' ) };
			$.ajax(options);
		}   
		
	//=-=-=-=-=-=-=-=-=-=-=-=-=-=--=-=-=-
		
		var qAPI={
			//-- send broadcast event:
			notify: function( evt_name, data ){
				if( evt_name && evt_name.length >0 ) jQ("#eventbus").triggerHandler(evt_name, [data]);
			},
			
			sendAjax: function( httpmethod, url, remote_svc_name, data, event_ok, event_error ){
				if ( remote_svc_name ) data.service=remote_svc_name; //include service name into var~s
				var result="";
				var response={};
				var options={};
				
				options["type"]= httpmethod;
				options["beforeSend"]= function(){};// instead of afterSend :)
				options["complete"]= function( _void, status ) { result=status };
				//~ options["cache"]= false;
				options["contentType"]= "application/x-www-form-urlencoded; charset=UTF-8";
				options["crossDomain"]= true;
				options["data"]= data;
				options["error"]= function(){ qAPI.notify( event_error ) };
				options["success"]= function(resp_data){ response=resp_data; qAPI.notify( event_ok ) };
				options["timeout"]= 300;
				
				qAPI.notify( "qAPI.ajax_begin" );
				jQ.ajax( url, options );
				qAPI.notify( "qAPI.ajax_end", result );
				return response;
			},
			// shortcut for GET method
			get: function( url, remote_svc_name, data, event_ok, event_error ) {	
				return qAPI.sendAjax( "GET",  url, remote_svc_name, data, event_ok, event_error );
			},
			post: function( url, remote_svc_name, data, event_ok, event_error ){			
				return qAPI.sendAjax( "POST",  url, remote_svc_name, data, event_ok, event_error );
			}
		}
		
		function prepareData( svarnames ){
			var vlist=svarnames.split(",");
			var data={};
			for ( var i=0; i<vlist.length; i++ ){
				data[vlist[i]]=( jQ("#"+vlist[i]).val() );
			}
			// add "console_name" and "user_name" here:
			// -----------------
			// "STUB":
			data["user_name"]="DZ";
			data["console_name"]="Test workpalce";
			// "STUB"^
			return data;
		}
		
	// hidden requests:
		
		function CHECK_SERVER_REQUEST(){
			qAPI.post("/endpoint.lua/api", "CHECK_SERVER_REQUEST", prepareData( "" ) );
		}
		
		function ENUM_USERS_REQUEST(){
			qAPI.post("/endpoint.lua/api", "ENUM_USERS_REQUEST", prepareData( "" ) );
		}

		function GET_CONSOLE_PRIVELEGES_REQUEST(){
			qAPI.post("/endpoint.lua/api", "GET_CONSOLE_PRIVELEGES_REQUEST", prepareData( "" ) );
		}

		function USER_LOGIN_REQUEST(){
			qAPI.post("/endpoint.lua/api", "USER_LOGIN_REQUEST", prepareData( "password" ) );
		}

		function USER_PRIVILEGES_REQUEST(){
			qAPI.post("/endpoint.lua/api", "USER_PRIVILEGES_REQUEST", prepareData( "password" ) );
		}

		function GET_CONSOLE_CFG_REQUEST(){
			qAPI.post("/endpoint.lua/api", "GET_CONSOLE_CFG_REQUEST", prepareData( "console_uid" ) ); // can be NULL
		}

		function SET_CONSOLE_CFG_COMMAND(){
			qAPI.post("/endpoint.lua/api", "SET_CONSOLE_CFG_COMMAND", prepareData( "console_uid,controller_name,console_name,autocall,timer_on,timer_tick" ) );
		}

		function CONSOLE_STARTUP_NOTIFICATION(){
			qAPI.post("/endpoint.lua/api", "CONSOLE_STARTUP_NOTIFICATION", prepareData( "" ) );
		}

		function CONSOLE_SHUTDOWN_NOTIFICATION(){
			qAPI.post("/endpoint.lua/api", "CONSOLE_SHUTDOWN_NOTIFICATION", prepareData( "" ) );
		}
		
		function ENUM_AVAILABLE_OPERATIONS_REQUEST(){
			qAPI.post("/endpoint.lua/api", "ENUM_AVAILABLE_OPERATIONS_REQUEST", prepareData( "" ) );
		}
///

		function CONSOLE_HEARTBEAT_NOTIFICATION(){
			qAPI.post("/endpoint.lua/api", "CONSOLE_HEARTBEAT_NOTIFICATION", prepareData( "" ) );
		}

		function ENUM_QUEUE_REQUEST(){
			qAPI.post("/endpoint.lua/api", "ENUM_QUEUE_REQUEST", prepareData( "" ) );
		}
		
		function render_servicelist(){
			var interior=qAPI.get("/endpoint.lua/widgets/servicelist", null, null);
			jQ("#tag").html(interior);
		}


	//^hidden requests^

		function smartLoader(url){
			jQ('#inet_frame').attr('src', 'wwwait.html');
			//Application.timer.pause();
			jQ('#webframe').show(); 
			//test connection before
			getContent('#none',url, null,
				// successfull connection:
				function(){
					jQ('#inet_frame').attr('src', url);
				},
				// error:
				function(){
					jQ('#inet_frame').attr('src', 'noconnect.html');
				}
			)
		}    
		

	
		var jQ = jQuery;
	
		jQ(function(){
		
//			lcamino.plugins.button.setSelector('.float-button');
		
		  jQ(".button").bind("mousedown", function(){
			jQ(this).addClass("pressed");
		  });  
		  jQ(".button").bind("mouseup", function(){
			jQ(this).removeClass("pressed");
		  });  
		  


		//bn-console-pause    /** ��������� ����� **/
		//bn-console-resume   /** �������� ����� **/
		//bn-admin-call   /** ������� �������������� **/
		//bn-browse-queue /** ����� �� ������... **/
		//bn-queue-next   /** ������� ���������� ������� **/
		//bn-queue-remove-item    /** ������� ������� �� ������� **/
		//bn-queue-bypass-item    /** ���������� ������� **/
		//bn-queue-item-noresponce    /** ���������� ������ �� ������� **/
		//bn-queue-item-processing-begin  /** ������ ����������� ������� **/
		//bn-queue-item-processing-end    /** ��������� ������������ ������� **/
		//bn-queue-item-processing-defer  /** �������� ������������ ������� �� ___ ����� **/
		//bn-queue-item-redirect  /** ������������� �� �������� ___ **/
		//bn-queue-item-call-by-id    /** ������� ������� �� ������ ������ ___ **/
		//bn-change-marquee-text  /** �������� ���������� � ������� ������... **/

		////////// get: function( remote_svc_name, data, event_ok, event_error ) 

		jQ("#bn-console-pause").click( function(){
			qAPI.post("SESSION_STANDBY_NOTIFICATION", prepareData( "" ) );
		});
		
		jQ("#bn-console-resume").click( function(){
			qAPI.post("SESSION_RESUME_NOTIFICATION", prepareData( "" ) );
		});
		
		jQ("#bn-admin-call").click( function(){
			qAPI.post("", prepareData( "" ) );
		});
		
		jQ("#bn-queue-next").click( function(){
			qAPI.post("DO_CALL_NEXT_ITEM_COMMAND", prepareData( "" ) );
		});
		
		jQ("#bn-queue-item-call-by-id").click( function(){
			qAPI.post("DO_CALL_ITEM_BY_ID_COMMAND", prepareData( "item_id" ) );
		});
		
		jQ("#bn-browse-queue").click( function(){
			qAPI.post("", prepareData( "" ) );
		});
		
		jQ("#bn-queue-remove-item").click( function(){
			qAPI.post("DO_REMOVE_ITEM_COMMAND", prepareData( "" ) );
		});
		
		jQ("#bn-queue-bypass-item").click( function(){
			qAPI.post("DO_ITEM_BYPASS_COMMAND", prepareData( "" ) );
		});
		
		jQ("#bn-queue-item-noresponce").click( function(){
			qAPI.post("DO_PROMPT_IGNORED_COMMAND", prepareData( "" ) );
		});
		
		jQ("#bn-queue-item-processing-begin").click( function(){
			qAPI.post("DO_ITEM_PROCESSING_BEGIN_COMMAND", prepareData( "" ) );
		});
		
		jQ("#bn-queue-item-processing-end").click( function(){
			qAPI.post("DO_ITEM_PROCESSING_END_COMMAND", prepareData( "" ) );
		});
		
		jQ("#bn-queue-item-processing-defer").click( function(){
			qAPI.post("DO_DEFER_ITEM_COMMAND", prepareData( "delay" ) );
		});
		
		jQ("#bn-queue-item-redirect").click( function(){
			qAPI.post("DO_REDIRECT_ITEM_COMMAND", prepareData( "tag" ) );
		});
		
		jQ("#bn-change-marquee-text").click( function(){
			qAPI.post("", prepareData( "" ) );
		});
		
		render_servicelist();
		
		// Autologon
		
		USER_LOGIN_REQUEST();
		USER_PRIVILEGES_REQUEST();

		// install timer for heartbeat
		var heartbeat_timer=null;
		var tick_count=0;
		heartbeat_timer = setInterval( function(){
			tick_count++;
			if (tick_count <= 30) {
				CONSOLE_HEARTBEAT_NOTIFICATION();
			} else {
				clearInterval( heartbeat_timer ) ;
				heartbeat_timer=setInterval( CONSOLE_HEARTBEAT_NOTIFICATION , 5000 )
			}
		}, 10000 );

		  //~ // *.bn-aeroflot, *.bn-comstar, *.bn-mts
		  //~ jQ(".bn-aeroflot").click( function(){
			//~ smartLoader("http://aeroflot.ru/main.aspx");
		  //~ });


		  //~ jQ(".bn-comstar").click( function(){
			//~ smartLoader("http://www.comstar-uts.ru/ru/services/");
		  //~ });
		  
		  //~ jQ(".bn-mts").click( function(){
			//~ smartLoader("http://www.mts.ru");
		  //~ });

		  //~ jQ(".bn-svo3").click( function(){
			//~ smartLoader("http://svo3.ru/ru/B2B/b2b_virtual_terminal/");
		  //~ });

		  //~ jQ(".bn-hotels").click( function(){
			//~ smartLoader("hotels.html");
		  //~ });

		  //~ jQ("#bn-home").click(function(){ jQ("#webframe").hide() });
		  
		  //~ jQ("#bn-history").click(function(){ 
			//~ //window.frames["inet_frame"].history.back; 
			//~ //window.history.go(-1);
			//~ top.history.go(-1);
			//~ });
		  
		  //~ jQ(".bn-meeting").click( function(){
			//~ smartLoader("meeting.html");
		  //~ });
		  //~ /*debug:*/
		  //~ jQ(".bn-help").click( function(){
			//~ smartLoader("hotelform.html");
		  //~ });
		  
		  // Timer


		//~ if (!Application) var Application = {};
		//~ Application.timer = new lcm.Timer();


		//~ //$(document).ready(function(){
			//~ $(document).bind( 'mousedown.timer mouseup.timer mousemove.timer click.timer keydown.timer keyup.timer keypress.timer', function(){
			//~ Application.timer.reset();
			//~ })
		//~ //});				
		
		
		//~ Application.timerConfirmation = {
			//~ _autoexec1: $('#timer_confirmation').click( function(){ 
				//~ Application.timerConfirmation.cancel()
			//~ }),
			//~ _autoexec2: $(window).unload(function(){Application.timerConfirmation.tmpTimerID = null;}),
			//~ show: function(){
			//~ if ( $('#webframe').is(':visible') ) {
				//~ Application.timer.pause();
				//~ Application.timerConfirmation.tmpTimerTicks = 15;
				//~ $('#timer_confirmation').show();
				//~ Application.timerConfirmation.tmpTimerID = setInterval(Application.timerConfirmation._execute, 1000);
			//~ }
			//~ },
			//~ _hide: function(){
			//~ $('#timer_confirmation').hide();
			//~ if (Application.timerConfirmation.tmpTimerID) clearInterval(Application.timerConfirmation.tmpTimerID);
			//~ Application.timerConfirmation.tmpTimerID = null;
			//~ Application.timer.resume();
			//~ },
			//~ ok: function(){
			//~ Application.timerConfirmation._hide();
			//~ //Application.resetApp();
			//~ jQ("#webframe").hide();
			//~ },
			//~ cancel: function(){
			//~ Application.timerConfirmation._hide();
			//~ },
			//~ _execute: function(){
			//~ if ( Application.timerConfirmation.tmpTimerTicks > -1 ) {
				//~ $('#timer_confirmation').show();
				//~ $('#timer_confirmation_time_remains').text(''+Application.timerConfirmation.tmpTimerTicks+' ');
				//~ Application.timerConfirmation.tmpTimerTicks--;
			//~ } else {
				//~ Application.timerConfirmation.ok();
			//~ }
			//~ }
		//~ };
		

		//~ Application.timer.setCallback( /*Application.resetApp*/Application.timerConfirmation.show );
		//~ Application.timer.setTimeout(30000);
		//~ Application.timer.resume();


		})
		
		//~ function testchar(){
		  //~ var oIframe = document.getElementById('inet_frame');
		  //~ var IFrameDoc;
		  
			//~ if (oIframe.contentDocument) {
				//~ // For NS6
				//~ IFrameDoc = oIframe.contentDocument; 
			  //~ } else if (oIframe.contentWindow) {
				//~ // For IE5.5 and IE6
				//~ IFrameDoc = oIframe.contentWindow.document;
			  //~ } else if (oIframe.document) {
				//~ // For IE5
				//~ IFrameDoc = oIframe.document;
			  //~ } IFrameDoc = null/*else {
				//~ return true;
			  //~ }		  */
		  
			//~ jQ(IFrameDoc).find('body').append("<div id='temp_div' style='position:absolute;left:10px;top:10px;width:300px;height:100px;background-color:white;'>123</div>");
		//~ }