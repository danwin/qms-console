/** include static files upon document creation
* Copyright (c) danwin 2010
* usage:
* include this file to use include fucntion or specify single target file to include as a part of this file location, even makefile, which contains multiple includes
* example: <script src="runtime/include.js#path/makefile.js"></script>
* Functions: 
* include.script("path/scriptfile path2/scr.js path3/scr"); 
* Allowed delimiters - space or ";"
* You can specify filename with or without extension
* Note: The src attribute for this script must be in the following format:
* <path-to-this-script>?action=loadproject#<path-to-your-project-file> 
* include.css is analogue for css files. You can also collect includes of css into separate .js file and call it
*/

var include = new function(){

	function split_string_list( list ){
		return list.replace(/\;+/, " ").replace(/^\s+|\s+$/g, "").split(/\s+/g);
	};

	this.script = function( files_str, encoding ){
		var files = split_string_list( files_str );
		encoding = encoding || "utf-8";
		var file;
		for (var i=0, len = files.length; i < len; i++){
			file = files[i];
			if (!file.match(/\.js$/g)) file = file + ".js";
			document.write( "<script type='text/javascript' encoding='"+ encoding +"' src='"+file+"'></script>" );
			this.script.loaded_files.push( file );
		}
	};
	
	this.script.loaded_files = [];
	
	this.css = function( files_str, media ){
		var files = split_string_list( files_str );
		media = media || "screen,projection";
		var file;
		for (var i=0, len = files.length; i < len; i++){
			file = files[i];
			if (!file.match(/\.css$/g)) file = file + ".css";
			document.write( "<link rel='stylesheet' media='"+media+"' type='text/css' href='"+file+"' />" );
			this.css.loaded_files.push( file );
		}
	};
	
	this.css.loaded_files = [];
};


// startup code

(function(){

	var scripts = document.getElementsByTagName('script');
	for (var i in scripts){
		// search include with matched src
		var src = scripts[i].src; // ? get attribute?
//		if ( src && src.match(/runtime\/include\.js\#/g) ){
		if ( src && src.match(/\?action\=loadproject\#/g) ){
			var make_script = src.split("#")[1];
			include.script(make_script);
		}
	}

})()








