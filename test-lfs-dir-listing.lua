-- my code
require "lfs"

function split( s, delimiter )
    assert( delimiter and #delimiter==1, 'Length of delimiter mus be exactly 1!' )
    local items, pattern = {}, '[^%' .. delimiter .. ']+' --  if delimiter is in magic char(s), escape it by '%'
    for k in string.gmatch( s, pattern ) do table.insert(items, k) end
    return items
end

function enum_files( path, ext )
    local list={}
    -- code borrowed from example of "lfs": http://keplerproject.github.io/luafilesystem/examples.html
    for file in lfs.dir(path) do -- plain list of directory files, ignore '.', '..' entries, ignore subdirs...
        if file ~= "." and file ~= ".." then
            local f = path..'/'..file
            --print ("\t "..f)
            local attr = lfs.attributes (f)
            assert (type(attr) == "table")
            if attr.mode ~= "directory" then
                
                local tags = split( file, '.' )
                print('--->\"'.. (tags[1] or '') .. '\", \"'..(tags[2] or '')..'\"' )
                
                local fname, fext = unpack (split( file, '.' ))
                if fext==ext then
                    print ( 'file name: '.. file .. '; w/o ext: '..fname )
                    table.insert( list, file )
                end
            end
        end
    end	
    return list				
end 
-- function


enum_files( 'X:\\��������', 'opr' )
os.exit()


-- Code by David Kastrup
require "lfs"

function dirtree(dir)
  assert(dir and dir ~= "", "directory parameter is missing or empty")
  if string.sub(dir, -1) == "/" then
    dir=string.sub(dir, 1, -2)
  end

  local function yieldtree(dir)
    for entry in lfs.dir(dir) do
      if entry ~= "." and entry ~= ".." then
        entry=dir.."/"..entry
	local attr=lfs.attributes(entry)
	coroutine.yield(entry,attr)
	if attr.mode == "directory" then
	  yieldtree(entry)
	end
      end
    end
  end

  return coroutine.wrap(function() yieldtree(dir) end)
end


function trimfextension( fname )
    
end

for filename, attr in dirtree( 'X:\\��������' ) do
    local function xpand(t)
        local s=''
        for key, val in pairs(t) do
            s=s..'   '..'; '..key..'>'..val..';   '
        end
        return s
    end
    print ( attr.mode, filename, xpand(attr))
end


