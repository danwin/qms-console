--[[main application file]]
-- must provide main proc (or callable object with __call) and with arguments (request, response)
-- you can use 'route' facility or directly process request here 

do -- incapsulate namespace :)
	require 'route' 
	
	require 'models'
	
	local json=require 'dkjson'
	
	local helpers=require 'helpers'
	
	
	--handler
--~ 	local function echo( request, response )
--~ 		response.headers['Content-Type']='text; charset=cp1251'
--~ 		local html='hello'
--~ 		response.write( html )		
--~ 	end
	
	-- handler
	local function jsonconnector( request, response )
		response.headers['Content-Type']='application/json; charset=cp1251'
		local vars = request.postvars
--~         response.write( json.encode(vars, {indent=true}) )
		local name=assert( vars.service, 'method name must be specified' )
		vars.service=nil -- remove "method" ???? 
		local result=callService( name, vars )
		local jsondata = json.encode( result )
		if result.ok then
			response.write( jsondata )
		else
			response.servererror( jsondata )
		end
--~         return 'Hello from Handler<br/>\n'..request.location.path..'<br/>\n'.. route.dump('\n<br/>')..'----end----' 
	end
	
	-- handler
	local function renderservicelist( request, response )
		response.headers['Content-Type']='text; charset=cp1251'
		local result=callService( 'ENUM_AVAILABLE_OPERATIONS_REQUEST', request.qvars )
		if not result.ok then 
			response.write( helpers.ERROR( result ) )
			return
		end
		local html=helpers.SELECT( result.result )
		response.write( html)
	end
	
	local function renderusrlist( request, response )
		response.headers['Content-Type']='text/html; charset=cp1251'
--~ 		response.write( request.env( "QUERY_STRING" )  )
--~ 		do return end
		local result=callService( 'ENUM_USERS_REQUEST', request.qvars )
		if not result.ok then 
			response.write( helpers.ERROR( result ) )
			return
		end
		local html=helpers.SELECT( result.result )
		response.write( html)	
	end
	
	local function renderqcontrol( request, response )
	end
	
	route.assign{
		
--~ 		{	'/static',	{'get'}, function( request, response ) 
--~ 			local path=
--~ 			local ok, html = pcall( io.open().text, "" )
--~ 			end
--~ 		},
	
		{   '/api',   {'post'},     jsonconnector  },
		
		{   '/widgets/qcontrol',   {'get'}, renderqcontrol },
		
		{   '/widgets/servicelist', {'get'}, renderservicelist, },
		
		{   '/widgets/usrlist', {'get'}, renderusrlist },
		
		{	'/echo',	{'get'}, function ( request, response )
				response.headers['Content-Type']='text; charset=cp1251'
				local html='hello'
				response.write( html )		
			end 
		}

	}
	
	
--~     route.assign('/', simplehandler)
--~     route.assign('/1', simplehandler, {'get'})
--~     route.assign('/1/2/3', simplehandler, {'get', 'put', 'post'})
--~     route.assign('/4', simplehandler, {'get'})

	--~ function execute(request, response)
	--~     response.write('Hello1')
	--~     response.write('Hello2')
	--~     response.write('Hello3')
	--~     response.write( route.dump('<br/>') )
	--~ end
--[[

local root=

{GET=f1,POST=get2,
	-- /root/api
	api={
		a1, a2
	},

	-- /root/rest
	rest={
		-- /root/rest/customer
		customer={GET=f1,POST=get2,
			
			-- /root/rest/customer/{id}
			_id={GET=f1,POST=get2,
				
				
			}
		},
		
		-- /root/rest/queue
		queue={
		},
		
		-- /root/rest/operator
		operator={
		}
	}
}

]]
		
	server.run(route.execute)
end