--[[main application file]]
-- must provide main proc (or callable object with __call) and with arguments (request, response)
-- you can use 'route' facility or directly process request here 

do -- incapsulate namespace :)
    require 'route' 

    function simplehandler()
        return 'Hello from Handler<br/>\n'..request.location.path..'<br/>\n'.. route.dump('\n<br/>')..'----end----' 
    end
    
    route.assign{
    
        {   '/',        simplehandler  },
        {   '/1',      simplehandler, {'get'} },
        {   '/1/2/3', simplehandler, {'get', 'put', 'post'} },
        {   '/4', simplehandler, {'get'} }
    }
    
    
--~     route.assign('/', simplehandler)
--~     route.assign('/1', simplehandler, {'get'})
--~     route.assign('/1/2/3', simplehandler, {'get', 'put', 'post'})
--~     route.assign('/4', simplehandler, {'get'})

    --~ function execute(request, response)
    --~     response.write('Hello1')
    --~     response.write('Hello2')
    --~     response.write('Hello3')
    --~     response.write( route.dump('<br/>') )
    --~ end
----[[

local root=

{GET=f1,POST=get2,
    -- /root/api
    api={
        a1, a2
    },

    -- /root/rest
    rest={
        -- /root/rest/customer
        customer={GET=f1,POST=get2,
            
            -- /root/rest/customer/{id}
            _id={GET=f1,POST=get2,
                
                
            }
        },
        
        -- /root/rest/queue
        queue={
        },
        
        -- /root/rest/operator
        operator={
        }
    }
}

--]]
        
    server.run(route.execute)
end