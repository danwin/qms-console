request = {}


require 'utils'


request.env = os.getenv

request.location={}
request.location.cgiscript = '/'..(arg and arg[0] or ''):match('[/\\]?([^/\\]+)$') -- /dispatch.lua
request.location.path = utils.uri.trimslash(request.env('PATH_INFO') or '') -- path reminder after cgiscript name (without query string), without leading and trailing slash(es) 
request.location.urn = request.location.cgiscript ..'/'.. request.location.path -- full path after uri authority, after url, Same as the absolute path.
request.location.controller = 'app' -- all logic must be in 'app.lua' or linked object module 'app'
--~ utils.uri.args = {select(2, request.location.path:match('[^/]+'))}
function request.location.resolve( path )     return path and request.location.urn..'/'..path or request.location.urn  end

request.location.argstr = request.location.path 

request.method = request.env("REQUEST_METHOD"):lower()
request.args = utils.uri.split( request.location.argstr ) -- array of path portions

local function extractfileext( urntags )
    local filename = urntags[#urntags] or ''
    local name, ext = unpack utils.uri.split( filename, '.' )
    return ext
end

request.location.fileext = extractfileext( request.args )
request.qvars = utils.decode_fields( request.env( "QUERY_STRING" ) or '' )
request.contentlength = tonumber(request.env("CONTENT_LENGTH")) or 0

request.postvars = ( request.method=='post' and request.contentlength>0 ) and utils.decode_fields(io.read (request.contentlength) or '') or {}

--debug
--~ print('HTTP/1.1: 200\r\n\r\n\r\n\n')
--~ print( '\n<br/>request.location.cgiscript: *** '..request.location.cgiscript )
--~ print( '\n<br/>request.location.path: *** '..request.location.path )
--~ print( '\n<br/>request.location.urn: *** '..request.location.urn )
--~ print( '\n<br/>request.location.controller: *** '..request.location.controller )
--~ print( '\n<br/>request.location.argstr: *** '..request.location.argstr )
--~ print( '\n<br/>request.args: *** ' )
--~ for k,v in pairs(request.args) do print('\n<br/>['..k..']='..v) end

--~ print( '\n<br/>request.postvars: *** ' )
--~ for k,v in pairs(request.postvars) do print('\n<br/>['..k..']='..v) end

return request