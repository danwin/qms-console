local spath = (arg and arg[0] or ''):gsub('[/\\]?[^/\\]+$', '') -- remove file name
if spath == '' then spath = '.' end
package.path = spath.."\\ext\\?.lua;"..spath.."\\ext\\?\\init.lua;"..spath.."\\?.lua;"..spath.."\\?\\init.lua"..package.path
package.cpath = spath.."\\ext\\?.dll;"..spath.."\\ext\\?\\core.dll;"..spath.."\\?.dll;"..package.cpath