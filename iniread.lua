--[[ini basic io]]

--[[
-- Danwin, #timestamp{2013-09-19}#
]]

--[[
--Copyright 2013 Dmitry Zimoglyadov (Danwin). All rights reserved.
-- 
-- Redistribution and use in source and binary forms, with or without modification, are
-- permitted provided that the following conditions are met:
-- 
--    1. Redistributions of source code must retain the above copyright notice, this list of
--       conditions and the following disclaimer.
-- 
--    2. Redistributions in binary form must reproduce the above copyright notice, this list
--       of conditions and the following disclaimer in the documentation and/or other materials
--       provided with the distribution.
-- 
-- THIS SOFTWARE IS PROVIDED BY BART VAN STRIEN ''AS IS'' AND ANY EXPRESS OR IMPLIED
-- WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
-- FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL BART VAN STRIEN OR
-- CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
-- SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
-- ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
-- NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
-- ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
-- The views and conclusions contained in the software and documentation are those of the
-- authors and should not be interpreted as representing official policies, either expressed
-- or implied, of Bart van Strien.
--
-- The above license is known as the Simplified BSD license.

]]  
  
iniread={}


function iniread.parse( name, parsers, t )
    local sectionparser, dataparser = parsers.onsection, parsers.ondata
	local section
    
    local	 function lines (name) return assert(io.open(name)):lines() end

	for line in lines(name)  do
		local s = line:match("^%[([^%]]+)%]$")
		if s then
			section = s
            if t and not t[section] then t[section] = {} end
			if sectionparser then sectionparser( section, t ) end
		end
		local key, value = line:match("^([%w_]+)%s-=%s-(.+)%s-$")
		if key and value then
			if tonumber(value) then value = tonumber(value) end
			if value == "true" then value = true end
			if value == "false" then value = false end
            if section then t[section][key] = value else t[key]=value end -- work with 2-level or plain table
			if dataparser then dataparser( key, value, section, t ) end
        end
	end
end

function iniread.load(name, parsers)
--[[
    @return 1.<boolean>true, 2.<table>data
    or: 1. <boolean>false, 2. <string>errmessage
]]
    local t={}
    local status, errmessage = pcall(iniread.parse, name, parsers, t)
    return status, status and t or errmessage
end
            
return iniread