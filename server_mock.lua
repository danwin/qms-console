--cgi test


app_to_test='endpoint' --omit .lua extension
request_method=arg[1] and string.upper(arg[1]) or 'GET'
--~ path_info='formio/panel/Panel 1'
path_info=arg[2] or '/4'--'1/2/3'



--post_vars=arg[3] and loadstring(arg[3]) or {newid=10, add='Add', select=9}
post_vars={newid=10, add='Add', select=9}


query_string=''


do
    function post_string()
--~         do
--~             return "newid=12&add='Add'"
--~         end
        
        local function escape(s)
            s = string.gsub(s, "([&=+%c])", function(c) return string.format("%%%02X", string.byte(c)) end)
            s = string.gsub(s, " ", "+")
            return s
        end     
        
        local t={}
        for k, v in pairs(post_vars) do
            t[#t+1] = k..'='..escape( v )
        end        
        return table.concat(t, '&')..'\n'
    end
    
    
    --redefine 
    io.read = function() 
        return post_string()
    end
end


do
    local _getenv = os.getenv
    local function getenv_(varname)
        if varname == 'REQUEST_METHOD' then return request_method end
        if varname == 'PATH_INFO' then return --[[app_to_test..'/'..]] path_info end
        if varname == 'QUERY_STRING' then return query_string end
        if varname == 'CONTENT_LENGTH' then return #(post_string())  end
        if varname == 'HTTP_REFERER' then return path_info end
        -- or path call to standard method
        return _getenv(varname)
    end
    os.getenv = getenv_
end

--app to test:
require(app_to_test)
