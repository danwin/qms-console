#!ljcgi.exe
--[[ dispatch ]]

__DEBUG__=true --debug mode, turn off in production
if __DEBUG__ then 
    io.stderr=io.stdout
    local syserror = error
    function error(msg)
        print(msg)
        syserror(msg)
    end
end

--set path for packages
require('setpath')

-- global request static object
request = require 'request'
-- global response static object
response = require 'response'
-- global output method, can be redefined later from application(e.g. for templating)
renderer = io.write 
-- global application object, must provide method: .execute( request, response )
application = nil 
appfname = 'main'

function mount( appfname)
    local status, result = pcall(require, appfname)
    if status then
        -- Note: application doesn't know about routing (notallowed, notfound, etc....)! All routing specifics will be in "routing" module (re-assign "application")
        application = result
        local handler = application.execute
        if handler then
            local status, message, httpcode, httpheaders = pcall(handler, request, response)
            if status then 
                if httpcode then response.status( httpcode, httpheaders ) end
                if message then -- if application returns response fields directly, instead of calling "response" methods
                    if type(message)=='string' then
                        response.write(message)
                    else 
                        response.servererror('Invalid response format: '..type(message))
                    end
                end
            else
                response.servererror(message)
            end        
        else -- handler ".execute" not found
            response.servererror('Invalid application: application module must export method \".execute( request, response )\"')
        end
    else
        response.servererror ("Cannot load main application: " .. result )
    end  
end

-- global function to process cgi request
function run( application )

end

local status, result = pcall(require, appfname)
if status then
    -- Note: application doesn't know about routing (notallowed, notfound, etc....)! All routing specifics will be in "routing" module (re-assign "application")
    application = result
    local handler = application.execute
    if handler then
        local status, message, httpcode, httpheaders = pcall(handler, request, response)
        if status then 
            if httpcode then response.status( httpcode, httpheaders ) end
            if message then -- if application returns response fields directly, instead of calling "response" methods
                if type(message)=='string' then
                    response.write(message)
                else 
                    response.servererror('Invalid response format: '..type(message))
                end
            end
        else
            response.servererror(message)
        end        
    else -- handler ".execute" not found
        response.servererror('Invalid application: application module must export method \".execute( request, response )\"')
    end
else
    response.servererror ("Cannot load main application: " .. result )
end    

response.flush(renderer)